Teensy 3.1 Bare-metal firmware

Microsoft Windows installation

Requirements:
* CodeSourcery G++ Lite for ARM EABI suite
* Freescale Kinetis source file and docs
* Optional : a good IDE (Codelite, Visual Studio Express,...)

Note: CodeSourcery provides the full prebuilt cross-compilation toolchain and some standard tools like cs-make and cs-rm, so until you want to do funny things, you've got everything to start.