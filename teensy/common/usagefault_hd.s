/*
 * File:	usagefault_hd.s
 * Purpose:	Interrupt handler for usagefault
 *
 *
 */
	.syntax unified
	.thumb
	.cpu cortex-m4
	
	.section ".startup","x",%progbits
	.thumb_func
 
	.global UsageFault_Handler
	.extern husage_fault_handler_c	
 
UsageFault_Handler:
  TST LR, #4
  ITE EQ
  MRSEQ R0, MSP
  MRSNE R0, PSP
  B usage_fault_handler_c
  
  