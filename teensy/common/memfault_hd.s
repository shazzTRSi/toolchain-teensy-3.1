/*
 * File:	memfault_hd.s
 * Purpose:	Interrupt handler for memmanagefault
 *
 *
 */
	.syntax unified
	.thumb
	.cpu cortex-m4
	
	.section ".startup","x",%progbits
	.thumb_func
 
	.global MemManage_Handler
	.extern memmgt_fault_handler_c	
 
MemManage_Handler:
  TST LR, #4
  ITE EQ
  MRSEQ R0, MSP
  MRSNE R0, PSP
  B memmgt_fault_handler_c
  
  