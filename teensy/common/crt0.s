/*
 * File:	crt0.s
 * Purpose:	Lowest level routines for Kinetis.
 *
 * Notes:  This code is modified from the original from the Kinetis K20
 * package from Freescale (kinetis_50MHz_sc.zip).  Portions are also taken
 * from mk20d5.zip file provided to the community by Freescale employee
 * Vojtech Filip; see https://community.freescale.com/thread/315653
 *
 */
	.syntax unified
	.thumb

	.section ".interrupt_vector_table"
	.global __interrupt_vector_table
/*
 *  This interrupt vector table gets pulled in by the linker script
 *  into flash right before the startup code
 *
 *  The following set of vectors are required by all ARM devices.
 */
__interrupt_vector_table:
	.long _top_stack			/* marks the top of stack */
	.long Reset_Handler
	.long NMI_Handler
	.long HardFault_Handler
	.long MemManage_Handler
	.long BusFault_Handler
	.long UsageFault_Handler
	.long DefaultFault_Handler
	.long DefaultFault_Handler
	.long DefaultFault_Handler
	.long DefaultFault_Handler
	.long SVC_Handler
	.long DebugMon_Handler
	.long DefaultFault_Handler
	.long PendSV_Handler
	.long SysTick_Handler

/* External Interrupts */
    .long   DMA0_IRQHandler			/* DMA channel 0 transfer complete interrupt */
    .long   DMA1_IRQHandler			/* DMA channel 1 transfer complete interrupt */
    .long   DMA2_IRQHandler			/* DMA channel 2 transfer complete interrupt */
    .long   DMA3_IRQHandler			/* DMA channel 3 transfer complete interrupt */
    .long   DMA_Error_IRQHandler	/* DMA error interrupt */
    .long   Reserved21_IRQHandler	/* Reserved interrupt 21 */
    .long   FTFL_IRQHandler			/* FTFL interrupt */
    .long   Read_Collision_IRQHandler	/* Read collision interrupt */
    .long   LVD_LVW_IRQHandler		/* Low Voltage Detect, Low Voltage Warning */
    .long   LLW_IRQHandler			/* Low Leakage Wakeup */
    .long   Watchdog_IRQHandler		/* WDOG interrupt */
    .long   I2C0_IRQHandler			/* I2C0 interrupt */
    .long   SPI0_IRQHandler			/* SPI0 interrupt */
    .long   I2S0_Tx_IRQHandler		/* I2S0 transmit interrupt */
    .long   I2S0_Rx_IRQHandler		/* I2S0 receive interrupt */
    .long   UART0_LON_IRQHandler	/* UART0 LON interrupt */
    .long   UART0_RX_TX_IRQHandler  /* UART0 receive/transmit interrupt */
    .long   UART0_ERR_IRQHandler	/* UART0 error interrupt */
    .long   UART1_RX_TX_IRQHandler  /* UART1 receive/transmit interrupt */
    .long   UART1_ERR_IRQHandler	/* UART1 error interrupt */
    .long   UART2_RX_TX_IRQHandler  /* UART2 receive/transmit interrupt */
    .long   UART2_ERR_IRQHandler	/* UART2 error interrupt */
    .long   ADC0_IRQHandler			/* ADC0 interrupt */
    .long   CMP0_IRQHandler			/* CMP0 interrupt */
    .long   CMP1_IRQHandler			/* CMP1 interrupt */
    .long   FTM0_IRQHandler			/* FTM0 fault, overflow and channels interrupt */
    .long   FTM1_IRQHandler			/* FTM1 fault, overflow and channels interrupt */
    .long   CMT_IRQHandler			/* CMT interrupt */
    .long   RTC_IRQHandler			/* RTC interrupt */
    .long   RTC_Seconds_IRQHandler  /* RTC seconds interrupt */
    .long   PIT0_IRQHandler			/* PIT timer channel 0 interrupt */
    .long   PIT1_IRQHandler			/* PIT timer channel 1 interrupt */
    .long   PIT2_IRQHandler			/* PIT timer channel 2 interrupt */
    .long   PIT3_IRQHandler			/* PIT timer channel 3 interrupt */
    .long   PDB0_IRQHandler			/* PDB0 interrupt */
    .long   USB0_IRQHandler			/* USB0 interrupt */
    .long   USBDCD_IRQHandler		/* USBDCD interrupt */
    .long   TSI0_IRQHandler			/* TSI0 interrupt */
    .long   MCG_IRQHandler			/* MCG interrupt */
    .long   LPTimer_IRQHandler		/* LPTimer interrupt */
    .long   PORTA_IRQHandler		/* Port A interrupt */
    .long   PORTB_IRQHandler		/* Port B interrupt */
    .long   PORTC_IRQHandler		/* Port C interrupt */
    .long   PORTD_IRQHandler		/* Port D interrupt */
    .long   PORTE_IRQHandler		/* Port E interrupt */
    .long   SWI_IRQHandler			/* Software interrupt */
    .long   DefaultISR				/* 62 */
    .long   DefaultISR				/* 63 */
    .long   DefaultISR				/* 64 */
    .long   DefaultISR				/* 65 */
    .long   DefaultISR				/* 66 */
    .long   DefaultISR				/* 67 */
    .long   DefaultISR				/* 68 */
    .long   DefaultISR				/* 69 */
    .long   DefaultISR				/* 70 */
    .long   DefaultISR				/* 71 */
    .long   DefaultISR				/* 72 */
    .long   DefaultISR				/* 73 */
    .long   DefaultISR				/* 74 */
    .long   DefaultISR				/* 75 */
    .long   DefaultISR				/* 76 */
    .long   DefaultISR				/* 77 */
    .long   DefaultISR				/* 78 */
    .long   DefaultISR				/* 79 */
    .long   DefaultISR				/* 80 */
    .long   DefaultISR				/* 81 */
    .long   DefaultISR				/* 82 */
    .long   DefaultISR				/* 83 */
    .long   DefaultISR				/* 84 */
    .long   DefaultISR				/* 85 */
    .long   DefaultISR				/* 86 */
    .long   DefaultISR				/* 87 */
    .long   DefaultISR				/* 88 */
    .long   USBOTG_IRQHandler			/* 89 */
    .long   DefaultISR				/* 90 */
    .long   DefaultISR				/* 91 */
    .long   DefaultISR				/* 92 */
    .long   DefaultISR				/* 93 */
    .long   DefaultISR				/* 94 */
    .long   DefaultISR				/* 95 */
    .long   DefaultISR				/* 96 */
    .long   DefaultISR				/* 97 */
    .long   DefaultISR				/* 98 */
    .long   DefaultISR				/* 99 */
    .long   DefaultISR				/* 100 */
    .long   DefaultISR				/* 101 */
    .long   DefaultISR				/* 102 */
    .long   DefaultISR				/* 103 */
    .long   DefaultISR				/* 104 */
    .long   DefaultISR				/* 105 */
    .long   DefaultISR				/* 106 */
    .long   DefaultISR				/* 107 */
    .long   DefaultISR				/* 108 */
    .long   DefaultISR				/* 109 */
    .long   DefaultISR				/* 110 */
    .long   DefaultISR				/* 111 */
    .long   DefaultISR				/* 112 */
    .long   DefaultISR				/* 113 */
    .long   DefaultISR				/* 114 */
    .long   DefaultISR				/* 115 */
    .long   DefaultISR				/* 116 */
    .long   DefaultISR				/* 117 */
    .long   DefaultISR				/* 118 */
    .long   DefaultISR				/* 119 */
    .long   DefaultISR				/* 120 */
    .long   DefaultISR				/* 121 */
    .long   DefaultISR				/* 122 */
    .long   DefaultISR				/* 123 */
    .long   DefaultISR				/* 124 */
    .long   DefaultISR				/* 125 */
    .long   DefaultISR				/* 126 */
    .long   DefaultISR				/* 127 */
    .long   DefaultISR				/* 128 */
    .long   DefaultISR				/* 129 */
    .long   DefaultISR				/* 130 */
    .long   DefaultISR				/* 131 */
    .long   DefaultISR				/* 132 */
    .long   DefaultISR				/* 133 */
    .long   DefaultISR				/* 134 */
    .long   DefaultISR				/* 135 */
    .long   DefaultISR				/* 136 */
    .long   DefaultISR				/* 137 */
    .long   DefaultISR				/* 138 */
    .long   DefaultISR				/* 139 */
    .long   DefaultISR				/* 140 */
    .long   DefaultISR				/* 141 */
    .long   DefaultISR				/* 142 */
    .long   DefaultISR				/* 143 */
    .long   DefaultISR				/* 144 */
    .long   DefaultISR				/* 145 */
    .long   DefaultISR				/* 146 */
    .long   DefaultISR				/* 147 */
    .long   DefaultISR				/* 148 */
    .long   DefaultISR				/* 149 */
    .long   DefaultISR				/* 150 */
    .long   DefaultISR				/* 151 */
    .long   DefaultISR				/* 152 */
    .long   DefaultISR				/* 153 */
    .long   DefaultISR				/* 154 */
    .long   DefaultISR				/* 155 */
    .long   DefaultISR				/* 156 */
    .long   DefaultISR				/* 157 */
    .long   DefaultISR				/* 158 */
    .long   DefaultISR				/* 159 */
    .long   DefaultISR				/* 160 */
    .long   DefaultISR				/* 161 */
    .long   DefaultISR				/* 162 */
    .long   DefaultISR				/* 163 */
    .long   DefaultISR				/* 164 */
    .long   DefaultISR				/* 165 */
    .long   DefaultISR				/* 166 */
    .long   DefaultISR				/* 167 */
    .long   DefaultISR				/* 168 */
    .long   DefaultISR				/* 169 */
    .long   DefaultISR				/* 170 */
    .long   DefaultISR				/* 171 */
    .long   DefaultISR				/* 172 */
    .long   DefaultISR				/* 173 */
    .long   DefaultISR				/* 174 */
    .long   DefaultISR				/* 175 */
    .long   DefaultISR				/* 176 */
    .long   DefaultISR				/* 177 */
    .long   DefaultISR				/* 178 */
    .long   DefaultISR				/* 179 */
    .long   DefaultISR				/* 180 */
    .long   DefaultISR				/* 181 */
    .long   DefaultISR				/* 182 */
    .long   DefaultISR				/* 183 */
    .long   DefaultISR				/* 184 */
    .long   DefaultISR				/* 185 */
    .long   DefaultISR				/* 186 */
    .long   DefaultISR				/* 187 */
    .long   DefaultISR				/* 188 */
    .long   DefaultISR				/* 189 */
    .long   DefaultISR				/* 190 */
    .long   DefaultISR				/* 191 */
    .long   DefaultISR				/* 192 */
    .long   DefaultISR				/* 193 */
    .long   DefaultISR				/* 194 */
    .long   DefaultISR				/* 195 */
    .long   DefaultISR				/* 196 */
    .long   DefaultISR				/* 197 */
    .long   DefaultISR				/* 198 */
    .long   DefaultISR				/* 199 */
    .long   DefaultISR				/* 200 */
    .long   DefaultISR				/* 201 */
    .long   DefaultISR				/* 202 */
    .long   DefaultISR				/* 203 */
    .long   DefaultISR				/* 204 */
    .long   DefaultISR				/* 205 */
    .long   DefaultISR				/* 206 */
    .long   DefaultISR				/* 207 */
    .long   DefaultISR				/* 208 */
    .long   DefaultISR				/* 209 */
    .long   DefaultISR				/* 210 */
    .long   DefaultISR				/* 211 */
    .long   DefaultISR				/* 212 */
    .long   DefaultISR				/* 213 */
    .long   DefaultISR				/* 214 */
    .long   DefaultISR				/* 215 */
    .long   DefaultISR				/* 216 */
    .long   DefaultISR				/* 217 */
    .long   DefaultISR				/* 218 */
    .long   DefaultISR				/* 219 */
    .long   DefaultISR				/* 220 */
    .long   DefaultISR				/* 221 */
    .long   DefaultISR				/* 222 */
    .long   DefaultISR				/* 223 */
    .long   DefaultISR				/* 224 */
    .long   DefaultISR				/* 225 */
    .long   DefaultISR				/* 226 */
    .long   DefaultISR				/* 227 */
    .long   DefaultISR				/* 228 */
    .long   DefaultISR				/* 229 */
    .long   DefaultISR				/* 230 */
    .long   DefaultISR				/* 231 */
    .long   DefaultISR				/* 232 */
    .long   DefaultISR				/* 233 */
    .long   DefaultISR				/* 234 */
    .long   DefaultISR				/* 235 */
    .long   DefaultISR				/* 236 */
    .long   DefaultISR				/* 237 */
    .long   DefaultISR				/* 238 */
    .long   DefaultISR				/* 239 */
    .long   DefaultISR				/* 240 */
    .long   DefaultISR				/* 241 */
    .long   DefaultISR				/* 242 */
    .long   DefaultISR				/* 243 */
    .long   DefaultISR				/* 244 */
    .long   DefaultISR				/* 245 */
    .long   DefaultISR				/* 246 */
    .long   DefaultISR				/* 247 */
    .long   DefaultISR				/* 248 */
    .long   DefaultISR				/* 249 */
    .long   DefaultISR				/* 250 */
    .long   DefaultISR				/* 251 */
    .long   DefaultISR				/* 252 */
    .long   DefaultISR				/* 253 */
    .long   DefaultISR				/* 254 */
    .long   DefaultISR				/* 255 */


/* Flash Configuration */

  	.long	0xFFFFFFFF
  	.long	0xFFFFFFFF
  	.long	0xFFFFFFFF
  	.long	0xFFFFFFFE

	.thumb


/*
 *  Now declare all interrupts handlers except the reset & fault handler
 *  to be weak.  This allows you to create a replacement handler
 *  in C with the same name and install it in the vector table
 *  above at link time.
 */

	.weak	NMI_Handler

	.weak	SVC_Handler
	.weak	DebugMon_Handler
	.weak	PendSV_Handler
	.weak	SysTick_Handler

    .weak   DMA0_IRQHandler				/* DMA channel 0 transfer complete interrupt */
    .weak   DMA1_IRQHandler				/* DMA channel 1 transfer complete interrupt */
    .weak   DMA2_IRQHandler				/* DMA channel 2 transfer complete interrupt */
    .weak   DMA3_IRQHandler				/* DMA channel 3 transfer complete interrupt */
    .weak   DMA_Error_IRQHandler		/* DMA error interrupt */
    .weak   Reserved21_IRQHandler		/* Reserved interrupt 21 */
    .weak   FTFL_IRQHandler				/* FTFL interrupt */
    .weak   Read_Collision_IRQHandler	/* Read collision interrupt */
    .weak   LVD_LVW_IRQHandler			/* Low Voltage Detect, Low Voltage Warning */
    .weak   LLW_IRQHandler				/* Low Leakage Wakeup */
    .weak   Watchdog_IRQHandler			/* WDOG interrupt */
    .weak   I2C0_IRQHandler				/* I2C0 interrupt */
    .weak   SPI0_IRQHandler				/* SPI0 interrupt */
    .weak   I2S0_Tx_IRQHandler			/* I2S0 transmit interrupt */
    .weak   I2S0_Rx_IRQHandler			/* I2S0 receive interrupt */
    .weak   UART0_LON_IRQHandler		/* UART0 LON interrupt */
    .weak   UART0_RX_TX_IRQHandler		/* UART0 receive/transmit interrupt */
    .weak   UART0_ERR_IRQHandler		/* UART0 error interrupt */
    .weak   UART1_RX_TX_IRQHandler		/* UART1 receive/transmit interrupt */
    .weak   UART1_ERR_IRQHandler		/* UART1 error interrupt */
    .weak   UART2_RX_TX_IRQHandler		/* UART2 receive/transmit interrupt */
    .weak   UART2_ERR_IRQHandler		/* UART2 error interrupt */
    .weak   ADC0_IRQHandler				/* ADC0 interrupt */
    .weak   CMP0_IRQHandler				/* CMP0 interrupt */
    .weak   CMP1_IRQHandler				/* CMP1 interrupt */
    .weak   FTM0_IRQHandler				/* FTM0 fault, overflow and channels interrupt */
    .weak   FTM1_IRQHandler				/* FTM1 fault, overflow and channels interrupt */
    .weak   CMT_IRQHandler				/* CMT interrupt */
    .weak   RTC_IRQHandler				/* RTC interrupt */
    .weak   RTC_Seconds_IRQHandler		/* RTC seconds interrupt */
    .weak   PIT0_IRQHandler				/* PIT timer channel 0 interrupt */
    .weak   PIT1_IRQHandler				/* PIT timer channel 1 interrupt */
    .weak   PIT2_IRQHandler				/* PIT timer channel 2 interrupt */
    .weak   PIT3_IRQHandler				/* PIT timer channel 3 interrupt */
    .weak   PDB0_IRQHandler				/* PDB0 interrupt */
    .weak   USB0_IRQHandler				/* USB0 interrupt */
    .weak   USBDCD_IRQHandler			/* USBDCD interrupt */
    .weak   TSI0_IRQHandler				/* TSI0 interrupt */
    .weak   MCG_IRQHandler				/* MCG interrupt */
    .weak   LPTimer_IRQHandler			/* LPTimer interrupt */
    .weak   PORTA_IRQHandler			/* Port A interrupt */
    .weak   PORTB_IRQHandler			/* Port B interrupt */
    .weak   PORTC_IRQHandler			/* Port C interrupt */
    .weak   PORTD_IRQHandler			/* Port D interrupt */
    .weak   PORTE_IRQHandler			/* Port E interrupt */
    .weak   SWI_IRQHandler				/* Software interrupt */
    .weak   DefaultISR
    


/*
 *  Actual code.
 */
	.section ".startup","x",%progbits
	.thumb_func
	.global _startup
	.global Reset_Handler


Reset_Handler:
_startup:
    mov     r0,#0                   /* Initialize the GPRs */
	mov     r1,#0
	mov     r2,#0
	mov     r3,#0
	mov     r4,#0
	mov     r5,#0
	mov     r6,#0
	mov     r7,#0
	mov     r8,#0
	mov     r9,#0
	mov     r10,#0
	mov     r11,#0
	mov     r12,#0

/*
 *  The CodeWarrior model wants to do system init in C rather than
 *  assembly.  But one of the system init functions is disabling the
 *  watchdog, which must happen within the first 256 clocks.  This
 *  conflicts with my desire to do all system init in assembler
 *  before jumping to main().
 *
 *  As a compromise, do an assembler jump to the wdog_disable() routine,
 *  then have that routine return here to finish low-level setup.
 */
	ldr r0, =wdog_disable
	blx r0

/*
 *  With the watchdog disabled, it is now safe to initialize areas
 *  of RAM without fear of watchdog timeout.
 *
 *  Clear the BSS section
 */ 
	mov r0, #0
	ldr r1, = _start_bss
	ldr r2, = _end_bss
	cmp	r1, r2
	beq	_done_clear

	sub r2, #1
_clear:
	cmp r1, r2
	str r0, [r1, #0]
	add r1, #4
	blo _clear
_done_clear:


/* 
 *  Copy data from flash initialization area to RAM
 *
 *  The three values seen here are supplied by the linker script
 */
    ldr   r0, =_start_data_flash	/* initial values, found in flash */
    ldr   r1, =_start_data			/* target locations in RAM to write */
    ldr   r2, =_data_size			/* number of bytes to write */

/*
 *  Perform the copy.
 *  Handle the special case where _data_size == 0
 */
    cmp   r2, #0
    beq   done_copy
copy:
    ldrb   r4, [r0], #1
    strb   r4, [r1], #1
    subs   r2, r2, #1
    bne    copy
done_copy:

/*
 *  Configure vector table offset register
 */
  ldr r0, =0xE000ED08
  ldr r1, =__interrupt_vector_table
  str r1, [r0]

/*
 *  Low-level init is done.  Time to hand off to the code that does
 *  mid-level init, such as setting up the PLL and any memory/gpio
 *  accesses.
 *
 *  Traditionally, this is done by invoking main() and letting main()
 *  do the setup.  The Freescale code (Code Warrior?) wants to use
 *  a start routine for mid-level setup; that routine then calls
 *  main().  OK, whatever...
 */
	ldr r0, =start
	blx r0
	b	.					/* just in case control ever leaves main()! */
	
/*
 *  The following stub routines serve as default handlers for the
 *  above vectors (except for the reset handler, of course).
 *
 *  You can recode this to have all dummy handlers use the same
 *  function, if you choose.  However, if you have a debugging
 *  tool, you should use a separate function for each default handler
 *  to make it easier to determine what caused any stray interrupt.
 */

/*
 *  Stub routines for the main vectors.
 */
NMI_Handler:
	b	.
	    
SVC_Handler:
	b	.
	    
DebugMon_Handler:
	b	.
	    
PendSV_Handler:
	b	.
	    
SysTick_Handler:
	b	.


/*
 *  Declare stub ISR handlers for the external interrupts.
 *
 *  If you have access to a debugger and need to isolate each of the
 *  handlers, simply follow each of the following entry points with
 *  a branch to self ("  b  .").
 */
DMA0_IRQHandler:
DMA1_IRQHandler:
DMA2_IRQHandler:
DMA3_IRQHandler:
DMA_Error_IRQHandler:
Reserved21_IRQHandler:
FTFL_IRQHandler:
Read_Collision_IRQHandler:
LVD_LVW_IRQHandler:
LLW_IRQHandler:
Watchdog_IRQHandler:
I2C0_IRQHandler:
SPI0_IRQHandler:
I2S0_Tx_IRQHandler:
I2S0_Rx_IRQHandler:
UART0_LON_IRQHandler:
UART0_RX_TX_IRQHandler:
UART0_ERR_IRQHandler:
UART1_RX_TX_IRQHandler:
UART1_ERR_IRQHandler:
UART2_RX_TX_IRQHandler:
UART2_ERR_IRQHandler:
ADC0_IRQHandler:
CMP0_IRQHandler:
CMP1_IRQHandler:
FTM0_IRQHandler:
FTM1_IRQHandler:
CMT_IRQHandler:
RTC_IRQHandler:
RTC_Seconds_IRQHandler:
PIT0_IRQHandler:
PIT1_IRQHandler:
PIT2_IRQHandler:
PIT3_IRQHandler:
PDB0_IRQHandler:
USB0_IRQHandler:
USBDCD_IRQHandler:
TSI0_IRQHandler:
MCG_IRQHandler:
LPTimer_IRQHandler:
PORTA_IRQHandler:
PORTB_IRQHandler:
PORTC_IRQHandler:
PORTD_IRQHandler:
PORTE_IRQHandler:
SWI_IRQHandler:
DefaultISR:
	b	.

DefaultFault_Handler:
	ldr r0, =fault_isr
	blx r0
	b	.
	
/*************************************************************/
/*
 *  Ignore this code.  It is never executed.
 *  I just stuck it here as a scratchpad, in case
 *  I need to restore code to disable the watchdog
 *  before calling sysinit().
 */
/*  Step 1 to unlock the WDOG, write 0xC520 to the unlock register */ 
	ldr	r3, WDOG_UNLOCK
	movw	r2, #50464
	strh	r2, [r3, #0]
/*  Step 2, write 0xD928 to the unlock register */	
	ldr	r3, WDOG_UNLOCK
	movw	r2, #55592
	strh	r2, [r3, #0]
	
/*  Now disable the WDOG by clearing the WDOGEN bit.  */
	ldr	r3, WDOG_STCTRLH
	ldr	r2, WDOG_STCTRLH
	ldrh	r2, [r2, #0]
	uxth	r2, r2
	bic	r2, r2, #1
	uxth	r2, r2
	strh	r2, [r3, #0]
/*
 *  If you use the above code, put the following constant defs somewhere
 *  where they won't get executed; they are used by the above code.
 */
WDOG_STCTRLH:
	.word	0x40052000
WDOG_UNLOCK:
	.word	0x4005200e


 
	.end
