/*
 * File:	usbotg.s
 * Purpose:	Interrupt handler for USB OTG Controller
 *
 *
 */
	.syntax unified
	.thumb
	.cpu cortex-m4
	
	.section ".startup","x",%progbits
	.thumb_func
 
	.global USBOTG_IRQHandler
	.extern usb_isr	
 
USBOTG_IRQHandler:
  B usb_isr
  
