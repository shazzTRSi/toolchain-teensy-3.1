// From Joseph Yiu, minor edits by FVH
// hard fault handler in C,
// with stack frame location as input parameter
// called from HardFault_Handler in file xxx.s

#include "term_io.h"


void defaultfault_handler_c (char * fault, unsigned int * fault_args)
{
  unsigned int stacked_r0;
  unsigned int stacked_r1;
  unsigned int stacked_r2;
  unsigned int stacked_r3;
  unsigned int stacked_r12;
  unsigned int stacked_lr;
  unsigned int stacked_pc;
  unsigned int stacked_psr;
 
  stacked_r0 = ((unsigned long) fault_args[0]);
  stacked_r1 = ((unsigned long) fault_args[1]);
  stacked_r2 = ((unsigned long) fault_args[2]);
  stacked_r3 = ((unsigned long) fault_args[3]);
 
  stacked_r12 = ((unsigned long) fault_args[4]);
  stacked_lr = ((unsigned long) fault_args[5]);
  stacked_pc = ((unsigned long) fault_args[6]);
  stacked_psr = ((unsigned long) fault_args[7]);
 
  xprintf ("\r\n\n");
  xprintf ("      ,--.!,  [%s Fault !]\r\n", fault);
  xprintf ("   __/   -*-  \r\n");
  xprintf (" ,d08b.  '|`  PC [r15]: 0x%08X\r\n", stacked_pc);
  xprintf (" 0088MM       LR [r14]: 0x%08X (Subroutine call return address)\r\n", stacked_lr);
  xprintf (" `9MMP'       \r\n");
  xprintf ("              PSR     : 0x%08X\tr0    : 0x%X\r\n", stacked_psr, stacked_r0);
  xprintf ("              BFAR    : 0x%08X\tr1    : 0x%X\r\n", (*((volatile unsigned long *)(0xE000ED38))), stacked_r1);
  xprintf ("              CFSR    : 0x%08X\tr2    : 0x%X\r\n", (*((volatile unsigned long *)(0xE000ED28))), stacked_r3);
  xprintf ("              HFSR    : 0x%08X\tr3    : 0x%X\r\n", (*((volatile unsigned long *)(0xE000ED2C))), stacked_r12);
  xprintf ("              DFSR    : 0x%08X\r\n", (*((volatile unsigned long *)(0xE000ED30))));
  xprintf ("              AFSR    : 0x%08X\r\n", (*((volatile unsigned long *)(0xE000ED30))));
  xprintf ("              MMAR    : 0x%08X\r\n", (*((volatile unsigned long *)(0xE000ED34))));  
  
  while (1);
}

void hard_fault_handler_c (unsigned int * fault_args)
{
	defaultfault_handler_c("Hard", fault_args);
}

void memmgt_fault_handler_c (unsigned int * fault_args)
{
	defaultfault_handler_c("MemMgt", fault_args);
}

void bus_fault_handler_c (unsigned int * fault_args)
{
	defaultfault_handler_c("Bus", fault_args);
}

void usage_fault_handler_c (unsigned int * fault_args)
{
	defaultfault_handler_c("Usage", fault_args);
}

/*!
* \file cortex_hardfault_handler.c
* \brief The code below implements a mechanism to discover HARD_FAULT sorces in Cortex-M embedded applications.
* \version mcufreaks.blogspot.com
*/
 
/*!
* \note The following declaration is mandatory to avoid compiler errors. \n
* In the declaration below, we are assigning the assembler label __label_hardfaultGetContext__
* to the entry point of __hardfaultGetContext__ function
*/
void hardfaultGetContext(unsigned long* stackedContextPtr) asm("label_hardfaultGetContext");	
 
/*!
* \fn void hardfaultGetContext(unsigned long* stackedContextPtr)
* \brief Copies system stacked context into function local variables. \n
* This function is called from asm-coded Interrupt Service Routine associated to HARD_FAULT exception
* \param stackedContextPtr : Address of stack containing stacked processor context.
*/
void hardfaultGetContext(unsigned long* stackedContextPtr)
{
	volatile unsigned long stacked_r0;
	volatile unsigned long stacked_r1;
	volatile unsigned long stacked_r2;
	volatile unsigned long stacked_r3;
	volatile unsigned long stacked_r12;
	volatile unsigned long stacked_lr;
	volatile unsigned long stacked_pc;
	volatile unsigned long stacked_psr;
	volatile unsigned long _CFSR;
	volatile unsigned long _HFSR;
	volatile unsigned long _DFSR;
	volatile unsigned long _AFSR;
	volatile unsigned long _BFAR;
	volatile unsigned long _MMAR;

	stacked_r0 = stackedContextPtr[0];
	stacked_r1 = stackedContextPtr[1];
	stacked_r2 = stackedContextPtr[2];
	stacked_r3 = stackedContextPtr[3];
	stacked_r12 = stackedContextPtr[4];
	stacked_lr = stackedContextPtr[5];
	stacked_pc = stackedContextPtr[6];
	stacked_psr = stackedContextPtr[7];
 
// Configurable Fault Status Register
// Consists of MMSR, BFSR and UFSR
	_CFSR = (*((volatile unsigned long *)(0xE000ED28))) ;
// Hard Fault Status Register
	_HFSR = (*((volatile unsigned long *)(0xE000ED2C))) ;
 
// Debug Fault Status Register
	_DFSR = (*((volatile unsigned long *)(0xE000ED30))) ;
 
// Auxiliary Fault Status Register
	_AFSR = (*((volatile unsigned long *)(0xE000ED3C))) ;
 
// Read the Fault Address Registers. These may not contain valid values.
// Check BFARVALID/MMARVALID to see if they are valid values
// MemManage Fault Address Register
	_MMAR = (*((volatile unsigned long *)(0xE000ED34))) ;
// Bus Fault Address Register
	_BFAR = (*((volatile unsigned long *)(0xE000ED38))) ;
 
//	__asm("BKPT #0\n") ; // Break into the debugger
	xprintf("\n\r*** Hard Fault ***");
	xprintf("\n\r R0:%08x  R1:%08x  R2:%08x  R3:%08x", stacked_r0, stacked_r1, stacked_r2, stacked_r3);
	xprintf("\n\rR12:%08x  LR:%08x  PC:%08x  PSR:%08x", stacked_r12, stacked_lr, stacked_pc, stacked_psr);
	xprintf("\n\rCFSR:%08x  HFSR:%08x  DFSR:%08x  AFSR:%08x", _CFSR, _HFSR, _DFSR, _AFSR);
	xprintf("\n\rMMAR:%08x  BFAR:%08x", _MMAR, _BFAR);

	while (1)  ;

// The following code avoids compiler warning [-Wunused-but-set-variable]
	stackedContextPtr[0] = stacked_r0;
	stackedContextPtr[1] = stacked_r1;
	stackedContextPtr[2] = stacked_r2;
	stackedContextPtr[3] = stacked_r3;
	stackedContextPtr[4] = stacked_r12;
	stackedContextPtr[5] = stacked_lr;
	stackedContextPtr[6] = stacked_pc;
	stackedContextPtr[7] = stacked_psr;
	(*((volatile unsigned long *)(0xE000ED28))) = _CFSR;
	(*((volatile unsigned long *)(0xE000ED2C))) = _HFSR;
	(*((volatile unsigned long *)(0xE000ED30))) = _DFSR;
	(*((volatile unsigned long *)(0xE000ED3C))) = _AFSR;
	(*((volatile unsigned long *)(0xE000ED34))) = _MMAR;
	(*((volatile unsigned long *)(0xE000ED38))) = _BFAR;
}
 
/*!
* \fn void hardfaultHandler(void)
* \brief HARD_FAULT interrupt service routine. Selects among PSP or MSP stacks and \n
* calls \ref hardfaultGetContext passing the selected stack pointer address as parameter.
* \note __naked__ attribute avoids generating prologue and epilogue code sequences generated \n
* for C-functions, and only pure asm instructions should be included into the function body.
*/
void __attribute__((naked, interrupt)) hardfault_isr(void)
{
__asm__ volatile	(
" MOVS R0, #4 \n" /* Determine if processor uses PSP or MSP by checking bit.4 at LR register. */
" MOV R1, LR \n"
" TST R0, R1 \n"
" BEQ _IS_MSP \n" /* Jump to '_MSP' if processor uses MSP stack. */
" MRS R0, PSP \n" /* Prepare PSP content as parameter to the calling function below. */
" BL label_hardfaultGetContext \n" /* Call 'hardfaultGetContext' passing PSP content as stackedContextPtr value. */
"_IS_MSP: \n"
" MRS R0, MSP \n" /* Prepare MSP content as parameter to the calling function below. */
" BL label_hardfaultGetContext \n" /* Call 'hardfaultGetContext' passing MSP content as stackedContextPtr value. */
::	);
}
