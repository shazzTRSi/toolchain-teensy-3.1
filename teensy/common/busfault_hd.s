/*
 * File:	busfault_hd.s
 * Purpose:	Interrupt handler for busfault
 *
 *
 */
	.syntax unified
	.thumb
	.cpu cortex-m4
	
	.section ".startup","x",%progbits
	.thumb_func
 
	.global BusFault_Handler
	.extern bus_fault_handler_c	
 
BusFault_Handler:
  TST LR, #4
  ITE EQ
  MRSEQ R0, MSP
  MRSNE R0, PSP
  B bus_fault_handler_c
  
  