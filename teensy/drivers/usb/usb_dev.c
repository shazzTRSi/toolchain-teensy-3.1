/* Teensyduino Core Library
 * http://www.pjrc.com/teensy/
 * Copyright (c) 2013 PJRC.COM, LLC.
 *
 */

//#include "mk20dx128.h"
#include "common.h"
#include "usb_dev.h"
#include "usb_desc.h"
#include "usb_mem.h"
#include "term_io.h"
#include "uart.h"

#include <string.h>// debug
void printUSBRegs(char * step);
void xprintBits(char * regName, uint32_t size, volatile void * ptr);

// buffer descriptor table
typedef struct {
	uint32_t desc;
	void * addr;
} bdt_t;

__attribute__ ((section(".usbdescriptortable"), used))
static bdt_t table[(NUM_ENDPOINTS+1)*4];

static usb_packet_t *rx_first[NUM_ENDPOINTS];
static usb_packet_t *rx_last[NUM_ENDPOINTS];
static usb_packet_t *tx_first[NUM_ENDPOINTS];
static usb_packet_t *tx_last[NUM_ENDPOINTS];
uint16_t usb_rx_byte_count_data[NUM_ENDPOINTS];

static uint8_t tx_state[NUM_ENDPOINTS];
#define TX_STATE_BOTH_FREE_EVEN_FIRST	0
#define TX_STATE_BOTH_FREE_ODD_FIRST	1
#define TX_STATE_EVEN_FREE		2
#define TX_STATE_ODD_FREE		3
#define TX_STATE_NONE_FREE_EVEN_FIRST	4
#define TX_STATE_NONE_FREE_ODD_FIRST	5

#define BDT_OWN		0x80
#define BDT_DATA1	0x40
#define BDT_DATA0	0x00
#define BDT_DTS		0x08
#define BDT_STALL	0x04
#define BDT_PID(n)	(((n) >> 2) & 15)

#define BDT_DESC(count, data)	(BDT_OWN | BDT_DTS \
				| ((data) ? BDT_DATA1 : BDT_DATA0) \
				| ((count) << 16))

#define TX   1
#define RX   0
#define ODD  1
#define EVEN 0
#define DATA0 0
#define DATA1 1
#define index(endpoint, tx, odd) (((endpoint) << 2) | ((tx) << 1) | (odd))
#define stat2bufferdescriptor(stat) (table + ((stat) >> 2))


static union {
 struct {
  union {
   struct {
	uint8_t bmRequestType;
	uint8_t bRequest;
   };
	uint16_t wRequestAndType;
  };
	uint16_t wValue;
	uint16_t wIndex;
	uint16_t wLength;
 };
 struct {
	uint32_t word1;
	uint32_t word2;
 };
} setup;


#define GET_STATUS		0
#define CLEAR_FEATURE		1
#define SET_FEATURE		3
#define SET_ADDRESS		5
#define GET_DESCRIPTOR		6
#define SET_DESCRIPTOR		7
#define GET_CONFIGURATION	8
#define SET_CONFIGURATION	9
#define GET_INTERFACE		10
#define SET_INTERFACE		11
#define SYNCH_FRAME		12

// SETUP always uses a DATA0 PID for the data field of the SETUP transaction.
// transactions in the data phase start with DATA1 and toggle (figure 8-12, USB1.1)
// Status stage uses a DATA1 PID.

static uint8_t ep0_rx0_buf[EP0_SIZE] __attribute__ ((aligned (4)));
static uint8_t ep0_rx1_buf[EP0_SIZE] __attribute__ ((aligned (4)));
static const uint8_t *ep0_tx_ptr = NULL;
static uint16_t ep0_tx_len;
static uint8_t ep0_tx_bdt_bank = 0;
static uint8_t ep0_tx_data_toggle = 0;
uint8_t usb_rx_memory_needed = 0;

volatile uint8_t usb_configuration = 0;
volatile uint8_t usb_reboot_timer = 0;

/*
 * void endpoint0_stall(void)
 */
static void endpoint0_stall(void)
{
	USB0_ENDPT0 = USB_ENDPT_EPSTALL_MASK | USB_ENDPT_EPRXEN_MASK | USB_ENDPT_EPTXEN_MASK | USB_ENDPT_EPHSHK_MASK;
}

/*
 * void endpoint0_transmit(const void *data, uint32_t len)
 */
static void endpoint0_transmit(const void *data, uint32_t len)
{
#if defined(SERIAL_DEBUG0)
	serial_print("tx0:");
	serial_phex32((uint32_t)data);
	serial_print(",");
	serial_phex16(len);
	serial_print(ep0_tx_bdt_bank ? ", odd" : ", even");
	serial_print(ep0_tx_data_toggle ? ", d1\n" : ", d0\n");
#endif
	table[index(0, TX, ep0_tx_bdt_bank)].addr = (void *)data;
	table[index(0, TX, ep0_tx_bdt_bank)].desc = BDT_DESC(len, ep0_tx_data_toggle);
	ep0_tx_data_toggle ^= 1;
	ep0_tx_bdt_bank ^= 1;
}

static uint8_t reply_buffer[8];

/*
 * void usb_setup(void)
 */
static void usb_setup(void)
{

#if defined(SERIAL_DEBUG)
	xputs("usb_setup\r\n");
#endif

	const uint8_t *data = NULL;
	uint32_t datalen = 0;
	const usb_descriptor_list_t *list;
	uint32_t size;
	volatile uint8_t *reg;
	uint8_t epconf;
	const uint8_t *cfg;
	int i;

	switch (setup.wRequestAndType) {
	  case 0x0500: // SET_ADDRESS
		break;
	  case 0x0900: // SET_CONFIGURATION
		//serial_print("configure\n");
		usb_configuration = setup.wValue;
		reg = &USB0_ENDPT1;
		cfg = usb_endpoint_config_table;
		// clear all BDT entries, free any allocated memory...
		for (i=4; i < (NUM_ENDPOINTS+1)*4; i++) {
			if (table[i].desc & BDT_OWN) {
				usb_free((usb_packet_t *)((uint8_t *)(table[i].addr) - 8));
			}
		}
		// free all queued packets
		for (i=0; i < NUM_ENDPOINTS; i++) {
			usb_packet_t *p, *n;
			p = rx_first[i];
			while (p) {
				n = p->next;
				usb_free(p);
				p = n;
			}
			rx_first[i] = NULL;
			rx_last[i] = NULL;
			p = tx_first[i];
			while (p) {
				n = p->next;
				usb_free(p);
				p = n;
			}
			tx_first[i] = NULL;
			tx_last[i] = NULL;
			usb_rx_byte_count_data[i] = 0;
			switch (tx_state[i]) {
			  case TX_STATE_EVEN_FREE:
			  case TX_STATE_NONE_FREE_EVEN_FIRST:
				tx_state[i] = TX_STATE_BOTH_FREE_EVEN_FIRST;
				break;
			  case TX_STATE_ODD_FREE:
			  case TX_STATE_NONE_FREE_ODD_FIRST:
				tx_state[i] = TX_STATE_BOTH_FREE_ODD_FIRST;
				break;
			  default:
				break;
			}
		}
		usb_rx_memory_needed = 0;
		for (i=1; i <= NUM_ENDPOINTS; i++) {
			epconf = *cfg++;
			*reg = epconf;
			reg += 4;
			if (epconf & USB_ENDPT_EPRXEN_MASK) {
				usb_packet_t *p;
				p = usb_malloc();
				if (p) {
					table[index(i, RX, EVEN)].addr = p->buf;
					table[index(i, RX, EVEN)].desc = BDT_DESC(64, 0);
				} else {
					table[index(i, RX, EVEN)].desc = 0;
					usb_rx_memory_needed++;
				}
				p = usb_malloc();
				if (p) {
					table[index(i, RX, ODD)].addr = p->buf;
					table[index(i, RX, ODD)].desc = BDT_DESC(64, 1);
				} else {
					table[index(i, RX, ODD)].desc = 0;
					usb_rx_memory_needed++;
				}
			}
			table[index(i, TX, EVEN)].desc = 0;
			table[index(i, TX, ODD)].desc = 0;
		}
		break;
	  case 0x0880: // GET_CONFIGURATION
		reply_buffer[0] = usb_configuration;
		datalen = 1;
		data = reply_buffer;
		break;
	  case 0x0080: // GET_STATUS (device)
		reply_buffer[0] = 0;
		reply_buffer[1] = 0;
		datalen = 2;
		data = reply_buffer;
		break;
	  case 0x0082: // GET_STATUS (endpoint)
		if (setup.wIndex > NUM_ENDPOINTS) {
			// TODO: do we need to handle IN vs OUT here?
			endpoint0_stall();
			return;
		}
		reply_buffer[0] = 0;
		reply_buffer[1] = 0;
		if (*(uint8_t *)(&USB0_ENDPT0 + setup.wIndex * 4) & 0x02) reply_buffer[0] = 1;
		data = reply_buffer;
		datalen = 2;
		break;
	  case 0x0102: // CLEAR_FEATURE (endpoint)
		i = setup.wIndex & 0x7F;
		if (i > NUM_ENDPOINTS || setup.wValue != 0) {
			// TODO: do we need to handle IN vs OUT here?
			endpoint0_stall();
			return;
		}
		(*(uint8_t *)(&USB0_ENDPT0 + setup.wIndex * 4)) &= ~0x02;
		// TODO: do we need to clear the data toggle here?
		break;
	  case 0x0302: // SET_FEATURE (endpoint)
		i = setup.wIndex & 0x7F;
		if (i > NUM_ENDPOINTS || setup.wValue != 0) {
			// TODO: do we need to handle IN vs OUT here?
			endpoint0_stall();
			return;
		}
		(*(uint8_t *)(&USB0_ENDPT0 + setup.wIndex * 4)) |= 0x02;
		// TODO: do we need to clear the data toggle here?
		break;
	  case 0x0680: // GET_DESCRIPTOR
	  case 0x0681:
		//serial_print("desc:");
		//serial_phex16(setup.wValue);
		//serial_print("\n");
		for (list = usb_descriptor_list; 1; list++) {
			if (list->addr == NULL) break;
			//if (setup.wValue == list->wValue &&
			//(setup.wIndex == list->wIndex) || ((setup.wValue >> 8) == 3)) {
			if (setup.wValue == list->wValue && setup.wIndex == list->wIndex) {
				data = list->addr;
				if ((setup.wValue >> 8) == 3) {
					// for string descriptors, use the descriptor's
					// length field, allowing runtime configured
					// length.
					datalen = *(list->addr);
				} else {
					datalen = list->length;
				}
#if defined(SERIAL_DEBUG)
				xputs("Desc found, ");
				xprintf("%d,%#016x %#08x \r\n", data, datalen, data[0]);
				//serial_phex(data[1]);
				//serial_phex(data[2]);
				//serial_phex(data[3]);
				//serial_phex(data[4]);
				//serial_phex(data[5]);
				//serial_print("\n");
#endif
				goto send;
			}
		}
		//serial_print("desc: not found\n");
		endpoint0_stall();
		return;
#if defined(CDC_STATUS_INTERFACE)
	  case 0x2221: // CDC_SET_CONTROL_LINE_STATE
		usb_cdc_line_rtsdtr = setup.wValue;
		//serial_print("set control line state\n");
		break;
	  case 0x2021: // CDC_SET_LINE_CODING
		//serial_print("set coding, waiting...\n");
		return;
#endif

// TODO: this does not work... why?
#if defined(SEREMU_INTERFACE) || defined(KEYBOARD_INTERFACE)
	  case 0x0921: // HID SET_REPORT
		//serial_print(":)\n");
		return;
	  case 0x0A21: // HID SET_IDLE
		break;
	  // case 0xC940:
#endif
	  default:
		endpoint0_stall();
		return;
	}
	send:

#if defined(SERIAL_DEBUG0)
	serial_print("setup send ");
	serial_phex32(data);
	serial_print(",");
	serial_phex16(datalen);
	serial_print("\n");
#endif

	if (datalen > setup.wLength) datalen = setup.wLength;
	size = datalen;
	if (size > EP0_SIZE) size = EP0_SIZE;
	endpoint0_transmit(data, size);
	data += size;
	datalen -= size;
	if (datalen == 0 && size < EP0_SIZE) return;

	size = datalen;
	if (size > EP0_SIZE) size = EP0_SIZE;
	endpoint0_transmit(data, size);
	data += size;
	datalen -= size;
	if (datalen == 0 && size < EP0_SIZE) return;

	ep0_tx_ptr = data;
	ep0_tx_len = datalen;
}



//A bulk endpoint's toggle sequence is initialized to DATA0 when the endpoint
//experiences any configuration event (configuration events are explained in
//Sections 9.1.1.5 and 9.4.5).

//Configuring a device or changing an alternate setting causes all of the status
//and configuration values associated with endpoints in the affected interfaces
//to be set to their default values. This includes setting the data toggle of
//any endpoint using data toggles to the value DATA0.

//For endpoints using data toggle, regardless of whether an endpoint has the
//Halt feature set, a ClearFeature(ENDPOINT_HALT) request always results in the
//data toggle being reinitialized to DATA0.



// #define stat2bufferdescriptor(stat) (table + ((stat) >> 2))

/*
 * void usb_control(uint32_t stat)
 */
static void usb_control(uint32_t stat)
{
	bdt_t *b;
	uint32_t pid, size;
	uint8_t *buf;
	const uint8_t *data;

	b = stat2bufferdescriptor(stat);
	pid = BDT_PID(b->desc);
	//count = b->desc >> 16;
	buf = b->addr;
#if defined(SERIAL_DEBUG0)
	serial_print("pid:");
	serial_phex(pid);
	serial_print(", count:");
	serial_phex(b->desc >> 16);
	serial_print("\n");
#endif

	switch (pid) {
	case 0x0D: // Setup received from host
		//serial_print("PID=Setup\n");
		//if (count != 8) ; // panic?
		// grab the 8 byte setup info
		setup.word1 = *(uint32_t *)(buf);
		setup.word2 = *(uint32_t *)(buf + 4);

		// give the buffer back
		b->desc = BDT_DESC(EP0_SIZE, DATA1);
		//table[index(0, RX, EVEN)].desc = BDT_DESC(EP0_SIZE, 1);
		//table[index(0, RX, ODD)].desc = BDT_DESC(EP0_SIZE, 1);

		// clear any leftover pending IN transactions
		ep0_tx_ptr = NULL;
		if (ep0_tx_data_toggle) {
		}
		//if (table[index(0, TX, EVEN)].desc & 0x80) {
			//serial_print("leftover tx even\n");
		//}
		//if (table[index(0, TX, ODD)].desc & 0x80) {
			//serial_print("leftover tx odd\n");
		//}
		table[index(0, TX, EVEN)].desc = 0;
		table[index(0, TX, ODD)].desc = 0;
		// first IN after Setup is always DATA1
		ep0_tx_data_toggle = 1;

#if defined(SERIAL_DEBUG0)
		serial_print("bmRequestType:");
		serial_phex(setup.bmRequestType);
		serial_print(", bRequest:");
		serial_phex(setup.bRequest);
		serial_print(", wValue:");
		serial_phex16(setup.wValue);
		serial_print(", wIndex:");
		serial_phex16(setup.wIndex);
		serial_print(", len:");
		serial_phex16(setup.wLength);
		serial_print("\n");
#endif
		// actually "do" the setup request
		usb_setup();
		// unfreeze the USB, now that we're ready
		USB0_CTL = USB_CTL_USBENSOFEN_MASK; // clear TXSUSPENDTOKENBUSY bit
		break;
	case 0x01:  // OUT transaction received from host
	case 0x02:
		//serial_print("PID=OUT\n");
#ifdef CDC_STATUS_INTERFACE
		if (setup.wRequestAndType == 0x2021 /*CDC_SET_LINE_CODING*/) {
			int i;
			uint8_t *dst = (uint8_t *)usb_cdc_line_coding;
			//serial_print("set line coding ");
			for (i=0; i<7; i++) {
				//serial_phex(*buf);
				*dst++ = *buf++;
			}
			//serial_phex32(usb_cdc_line_coding[0]);
			//serial_print("\n");
			if (usb_cdc_line_coding[0] == 134) usb_reboot_timer = 15;
			endpoint0_transmit(NULL, 0);
		}
#endif
#ifdef KEYBOARD_INTERFACE
		if (setup.word1 == 0x02000921 && setup.word2 == ((1<<16)|KEYBOARD_INTERFACE)) {
			keyboard_leds = buf[0];
			endpoint0_transmit(NULL, 0);
		}
#endif
#ifdef SEREMU_INTERFACE
		if (setup.word1 == 0x03000921 && setup.word2 == ((4<<16)|SEREMU_INTERFACE)
		  && buf[0] == 0xA9 && buf[1] == 0x45 && buf[2] == 0xC2 && buf[3] == 0x6B) {
			usb_reboot_timer = 5;
			endpoint0_transmit(NULL, 0);
		}
#endif
		// give the buffer back
		b->desc = BDT_DESC(EP0_SIZE, DATA1);
		break;

	case 0x09: // IN transaction completed to host
		//serial_print("PID=IN:");
		//serial_phex(stat);
		//serial_print("\n");

		// send remaining data, if any...
		data = ep0_tx_ptr;
		if (data) {
			size = ep0_tx_len;
			if (size > EP0_SIZE) size = EP0_SIZE;
			endpoint0_transmit(data, size);
			data += size;
			ep0_tx_len -= size;
			ep0_tx_ptr = (ep0_tx_len > 0 || size == EP0_SIZE) ? data : NULL;
		}

		if (setup.bRequest == 5 && setup.bmRequestType == 0) {
			setup.bRequest = 0;
			//serial_print("set address: ");
			//serial_phex16(setup.wValue);
			//serial_print("\n");
			USB0_ADDR = setup.wValue;
		}

		break;
	//default:
		//serial_print("PID=unknown:");
		//serial_phex(pid);
		//serial_print("\n");
	}
	USB0_CTL = USB_CTL_USBENSOFEN_MASK; // clear TXSUSPENDTOKENBUSY bit
}

/*
 * usb_packet_t *usb_rx(uint32_t endpoint)
 */
usb_packet_t *usb_rx(uint32_t endpoint)
{
	usb_packet_t *ret;
	endpoint--;
	if (endpoint >= NUM_ENDPOINTS) return NULL;
	__disable_irq();
	ret = rx_first[endpoint];
	if (ret) rx_first[endpoint] = ret->next;
	usb_rx_byte_count_data[endpoint] -= ret->len;
	__enable_irq();

#if defined(SERIAL_DEBUG0)
	serial_print("rx, epidx=");
	serial_phex(endpoint);
	serial_print(", packet=");
	serial_phex32(ret);
	serial_print("\n");
#endif
	return ret;
}

/*
 * static uint32_t usb_queue_byte_count(const usb_packet_t *p)
 */
static uint32_t usb_queue_byte_count(const usb_packet_t *p)
{
	uint32_t count=0;

	__disable_irq();
	for ( ; p; p = p->next) {
		count += p->len;
	}
	__enable_irq();
	return count;
}

// TODO: make this an inline function...
/*
uint32_t usb_rx_byte_count(uint32_t endpoint)
{
	endpoint--;
	if (endpoint >= NUM_ENDPOINTS) return 0;
	return usb_rx_byte_count_data[endpoint];
	//return usb_queue_byte_count(rx_first[endpoint]);
}
*/

/*
 * uint32_t usb_tx_byte_count(uint32_t endpoint)
 */
uint32_t usb_tx_byte_count(uint32_t endpoint)
{
	endpoint--;
	if (endpoint >= NUM_ENDPOINTS) return 0;
	return usb_queue_byte_count(tx_first[endpoint]);
}

/*
 * uint32_t usb_tx_packet_count(uint32_t endpoint)
 */
uint32_t usb_tx_packet_count(uint32_t endpoint)
{
	const usb_packet_t *p;
	uint32_t count=0;

	endpoint--;
	if (endpoint >= NUM_ENDPOINTS) return 0;
	__disable_irq();
	for (p = tx_first[endpoint]; p; p = p->next) count++;
	__enable_irq();
	return count;
}


// Called from usb_free, but only when usb_rx_memory_needed > 0, indicating
// receive endpoints are starving for memory.  The intention is to give
// endpoints needing receive memory priority over the user's code, which is
// likely calling usb_malloc to obtain memory for transmitting.  When the
// user is creating data very quickly, their consumption could starve reception
// without this prioritization.  The packet buffer (input) is assigned to the
// first endpoint needing memory.
//
/*
 * usb_rx_memory(usb_packet_t *packet)
 */
void usb_rx_memory(usb_packet_t *packet)
{
	unsigned int i;
	const uint8_t *cfg;

	cfg = usb_endpoint_config_table;
	//serial_print("rx_mem:");
	__disable_irq();
	for (i=1; i <= NUM_ENDPOINTS; i++) {
		if (*cfg++ & USB_ENDPT_EPRXEN_MASK) {
			if (table[index(i, RX, EVEN)].desc == 0) {
				table[index(i, RX, EVEN)].addr = packet->buf;
				table[index(i, RX, EVEN)].desc = BDT_DESC(64, 0);
				usb_rx_memory_needed--;
				__enable_irq();
				//serial_phex(i);
				//serial_print(",even\n");
				return;
			}
			if (table[index(i, RX, ODD)].desc == 0) {
				table[index(i, RX, ODD)].addr = packet->buf;
				table[index(i, RX, ODD)].desc = BDT_DESC(64, 1);
				usb_rx_memory_needed--;
				__enable_irq();
				//serial_phex(i);
				//serial_print(",odd\n");
				return;
			}
		}
	}
	__enable_irq();
	// we should never reach this point.  If we get here, it means
	// usb_rx_memory_needed was set greater than zero, but no memory
	// was actually needed.
	usb_rx_memory_needed = 0;
	usb_free(packet);
	return;
}

//#define index(endpoint, tx, odd) (((endpoint) << 2) | ((tx) << 1) | (odd))
//#define stat2bufferdescriptor(stat) (table + ((stat) >> 2))

/*
 * void usb_tx(uint32_t endpoint, usb_packet_t *packet)
 */
void usb_tx(uint32_t endpoint, usb_packet_t *packet)
{
	bdt_t *b = &table[index(endpoint, TX, EVEN)];
	uint8_t next;

	endpoint--;
	if (endpoint >= NUM_ENDPOINTS) return;
	__disable_irq();

#if defined(SERIAL_DEBUG0)
	serial_print("txstate=");
	serial_phex(tx_state[endpoint]);
	serial_print("\n");
#endif
	switch (tx_state[endpoint]) {
	  case TX_STATE_BOTH_FREE_EVEN_FIRST:
		next = TX_STATE_ODD_FREE;
		break;
	  case TX_STATE_BOTH_FREE_ODD_FIRST:
		b++;
		next = TX_STATE_EVEN_FREE;
		break;
	  case TX_STATE_EVEN_FREE:
		next = TX_STATE_NONE_FREE_ODD_FIRST;
		break;
	  case TX_STATE_ODD_FREE:
		b++;
		next = TX_STATE_NONE_FREE_EVEN_FIRST;
		break;
	  default:
		if (tx_first[endpoint] == NULL) {
			tx_first[endpoint] = packet;
		} else {
			tx_last[endpoint]->next = packet;
		}
		tx_last[endpoint] = packet;
		__enable_irq();
		return;
	}
	tx_state[endpoint] = next;
	b->addr = packet->buf;
	b->desc = BDT_DESC(packet->len, ((uint32_t)b & 8) ? DATA1 : DATA0);
	__enable_irq();
}



/*
 * void _reboot_Teensyduino_(void)
 */
void _reboot_Teensyduino_(void)
{
	// TODO: initialize R0 with a code....
	asm volatile("bkpt");
}


/*
 * void usb_isr(void)
 */
void usb_isr(void)
{
	uint8_t status, stat, t;
	
	//__disable_irq();

#if defined(SERIAL_DEBUG0)
	PORTC_PCR5 = PORT_PCR_MUX(0x1); // LED is on PC5 (pin 13), config as GPIO (alt = 1)
	GPIOC_PDDR = (1<<5);			// make this an output pin	
	GPIOC_PSOR=(1<<5);
	xputs("[usb_isr] start\r\n");
	status = USB0_ISTAT;
	xprintf("Status: %X\r\n", status);
#endif

	restart:
	status = USB0_ISTAT;

	if ((status & USB_INTEN_SOFTOKEN_MASK /* 04 */ )) {
		if (usb_configuration) {
			t = usb_reboot_timer;
			if (t) {
				usb_reboot_timer = --t;
				if (!t) _reboot_Teensyduino_();
			}
#ifdef CDC_DATA_INTERFACE
			t = usb_cdc_transmit_flush_timer;
			if (t) {
				usb_cdc_transmit_flush_timer = --t;
				if (t == 0) usb_serial_flush_callback();
			}
#endif
#ifdef SEREMU_INTERFACE
			t = usb_seremu_transmit_flush_timer;
			if (t) {
				usb_seremu_transmit_flush_timer = --t;
				if (t == 0) usb_seremu_flush_callback();
			}
#endif
#ifdef MIDI_INTERFACE
                        usb_midi_flush_output();
#endif
#ifdef FLIGHTSIM_INTERFACE
			usb_flightsim_flush_callback();
#endif
		}
		USB0_ISTAT = USB_INTEN_SOFTOKEN_MASK;
	}

	if ((status & USB_ISTAT_TOKDNE_MASK /* 08 */ )) {
		uint8_t endpoint;
		stat = USB0_STAT;

#if defined(SERIAL_DEBUG)
		xprintf("token: ep=%X, Stat: %X -> ", endpoint, stat >> 4);
		xprintf(stat & 0x08 ? ",tx" : ",rx");
		xprintf(stat & 0x04 ? ",odd\n" : ",even\r\n");
#endif
		endpoint = stat >> 4;
		if (endpoint == 0) {
			usb_control(stat);
		} else {
			bdt_t *b = stat2bufferdescriptor(stat);
			usb_packet_t *packet = (usb_packet_t *)((uint8_t *)(b->addr) - 8);
#if defined(SERIAL_DEBUG)
			xprintf("endpoint %X,  pid: %X -> ", endpoint, BDT_PID(b->desc));
			xprintf(((uint32_t)b & 8) ? ", odd" : ", even");
			xprintf(", Count: %X\r\n", b->desc >> 16);
#endif
			endpoint--;	// endpoint is index to zero-based arrays

			if (stat & 0x08) { // transmit
				usb_free(packet);
				packet = tx_first[endpoint];
				if (packet) {
#if defined(SERIAL_DEBUG)
					xprintf("tx packet\r\n");
#endif					
					tx_first[endpoint] = packet->next;
					b->addr = packet->buf;
					switch (tx_state[endpoint]) {
					  case TX_STATE_BOTH_FREE_EVEN_FIRST:
						tx_state[endpoint] = TX_STATE_ODD_FREE;
						break;
					  case TX_STATE_BOTH_FREE_ODD_FIRST:
						tx_state[endpoint] = TX_STATE_EVEN_FREE;
						break;
					  case TX_STATE_EVEN_FREE:
						tx_state[endpoint] = TX_STATE_NONE_FREE_ODD_FIRST;
						break;
					  case TX_STATE_ODD_FREE:
						tx_state[endpoint] = TX_STATE_NONE_FREE_EVEN_FIRST;
						break;
					  default:
						break;
					}
					b->desc = BDT_DESC(packet->len, ((uint32_t)b & 8) ? DATA1 : DATA0);
				} else {
#if defined(SERIAL_DEBUG)
					xprintf("tx no packet\r\n");
#endif
					switch (tx_state[endpoint]) {
					  case TX_STATE_BOTH_FREE_EVEN_FIRST:
					  case TX_STATE_BOTH_FREE_ODD_FIRST:
						break;
					  case TX_STATE_EVEN_FREE:
						tx_state[endpoint] = TX_STATE_BOTH_FREE_EVEN_FIRST;
						break;
					  case TX_STATE_ODD_FREE:
						tx_state[endpoint] = TX_STATE_BOTH_FREE_ODD_FIRST;
						break;
					  default:
						tx_state[endpoint] = ((uint32_t)b & 8) ?
						  TX_STATE_ODD_FREE : TX_STATE_EVEN_FREE;
						break;
					}
				}
			} else { // receive
				packet->len = b->desc >> 16;
				if (packet->len > 0) {
					packet->index = 0;
					packet->next = NULL;
					if (rx_first[endpoint] == NULL) {
#if defined(SERIAL_DEBUG)
						xprintf("rx 1st, epidx=%X, packet=%X\r\n", endpoint, (uint32_t)packet);
#endif							
						rx_first[endpoint] = packet;
					} else {
#if defined(SERIAL_DEBUG)
						xprintf("rx Nth, epidx=%X, packet=%X\r\n", endpoint, (uint32_t)packet);
#endif								
						rx_last[endpoint]->next = packet;
					}
					rx_last[endpoint] = packet;
					usb_rx_byte_count_data[endpoint] += packet->len;
					// TODO: implement a per-endpoint maximum # of allocated packets
					// so a flood of incoming data on 1 endpoint doesn't starve
					// the others if the user isn't reading it regularly
					packet = usb_malloc();
					if (packet) {
						b->addr = packet->buf;
						b->desc = BDT_DESC(64, ((uint32_t)b & 8) ? DATA1 : DATA0);
					} else {
#if defined(SERIAL_DEBUG)
						xprintf("starving %X -> ", endpoint + 1);
						xprintf(((uint32_t)b & 8) ? ",odd\n" : ",even\r\n");
#endif							
						b->desc = 0;
						usb_rx_memory_needed++;
					}
				} else {
					b->desc = BDT_DESC(64, ((uint32_t)b & 8) ? DATA1 : DATA0);
				}
			}
		}
		USB0_ISTAT = USB_ISTAT_TOKDNE_MASK;
		goto restart;
	}

	if (status & USB_ISTAT_USBRST_MASK /* 01 */ ) {
#if defined(SERIAL_DEBUG)
		xprintf("reset\r\n");
#endif	
		// initialize BDT toggle bits
		USB0_CTL = USB_CTL_ODDRST_MASK;
		ep0_tx_bdt_bank = 0;

		// set up buffers to receive Setup and OUT packets
		table[index(0, RX, EVEN)].desc = BDT_DESC(EP0_SIZE, 0);
		table[index(0, RX, EVEN)].addr = ep0_rx0_buf;
		table[index(0, RX, ODD)].desc = BDT_DESC(EP0_SIZE, 0);
		table[index(0, RX, ODD)].addr = ep0_rx1_buf;
		table[index(0, TX, EVEN)].desc = 0;
		table[index(0, TX, ODD)].desc = 0;

		// activate endpoint 0
		USB0_ENDPT0 = USB_ENDPT_EPRXEN_MASK | USB_ENDPT_EPTXEN_MASK | USB_ENDPT_EPHSHK_MASK;

		// clear all ending interrupts
		USB0_ERRSTAT = 0xFF;
		USB0_ISTAT = 0xFF;

		// set the address to zero during enumeration
		USB0_ADDR = 0;

		// enable other interrupts
		USB0_ERREN = 0xFF;
		USB0_INTEN = USB_INTEN_TOKDNEEN_MASK |
			USB_INTEN_SOFTOKEN_MASK |
			USB_INTEN_STALLEN_MASK |
			USB_INTEN_ERROREN_MASK |
			USB_INTEN_USBRSTEN_MASK |
			USB_INTEN_SLEEPEN_MASK;

		// is this necessary?
		USB0_CTL = USB_CTL_USBENSOFEN_MASK;
		return;
	}

	if ((status & USB_ISTAT_STALL_MASK /* 80 */ )) {
#if defined(SERIAL_DEBUG)
		xprintf("stall\r\n");
#endif	
		USB0_ENDPT0 = USB_ENDPT_EPRXEN_MASK | USB_ENDPT_EPTXEN_MASK | USB_ENDPT_EPHSHK_MASK;
		USB0_ISTAT = USB_ISTAT_STALL_MASK;
	}
	if ((status & USB_ISTAT_ERROR_MASK /* 02 */ )) {
		uint8_t err = USB0_ERRSTAT;
		USB0_ERRSTAT = err;
#if defined(SERIAL_DEBUG)
		xprintf("err: %X\r\n", err);
#endif			
		USB0_ISTAT = USB_ISTAT_ERROR_MASK;
	}

	if ((status & USB_ISTAT_SLEEP_MASK /* 10 */ )) {
#if defined(SERIAL_DEBUG)
		xprintf("sleep\r\n");
#endif	
		USB0_ISTAT = USB_ISTAT_SLEEP_MASK;
	}

}


/*
 * void usb_init(void)
 */
void usb_init(void)
{
	int i;

	usb_init_serialnumber();

	for (i=0; i <= NUM_ENDPOINTS*4; i++) {
		table[i].desc = 0;
		table[i].addr = 0;
	}
	
	// this basically follows the flowchart in the Kinetis
	// Quick Reference User Guide, Rev. 2, 08/2012, page 134
	// 1. Select Clock source
	// 2. USB clock gating (SIM_SCGC4)
	// 3. Reset USB module (USBx_USBTRC0)
	// 4. Set BDT base registers
	//    PAGE1 : Provides address bits 15 through 9 of the base address where the current Buffer Descriptor Table (BDT) resides in system memory.
	//    PAGE2 : Provides address bits 23 through 16 of the BDT base address that defines the location of Buffer Descriptor Table resides in system memory.
	//	  PAGE3 : Provides address bits 31 through 24 of the BDT base address that defines the location of Buffer Descriptor Table resides in system memory.
	// 5. Clear all USB ISR flags & enable weak pulldowns 
	//	  USBx_ISTAT, USB0_ERRSTAT, USB0_OTGISTAT :  After an interrupt bit has been set it may only be cleared by writing a one to the respective interrupt bit
	// 6. Enable USB Reset interrupt
	// 7. Enable pullup resistor
	// 8. Wait for host connection
	
	//xprintf("pouet\r\n");
	//xprintBits("SIM_SOPT2", 4, &SIM_SOPT2);
	
	// 1. configure USB for 48 MHz clock based on PLL
	SIM_CLKDIV2 = SIM_CLKDIV2_USBDIV(0); // USB = Divider output clock = Divider input clock × [ (USBFRAC+1) / (USBDIV+1) ] = DIC*(1/1) = 48 MHz PLL
	// USB uses PLL clock, trace is CPU clock, CLKOUT=OSCERCLK0
	SIM_SOPT2 = SIM_SOPT2_USBSRC_MASK | SIM_SOPT2_PLLFLLSEL_MASK | SIM_SOPT2_TRACECLKSEL_MASK | SIM_SOPT2_CLKOUTSEL(6);
	//SIM_SOPT2 = SIM_SOPT2_USBSRC_MASK | SIM_SOPT2_PLLFLLSEL_MASK;
	
	// enable interrupts
	/*
	__enable_irq();
	NVIC_ENABLE_IRQ(IRQ_PORTA);
	NVIC_ENABLE_IRQ(IRQ_PORTB);
	NVIC_ENABLE_IRQ(IRQ_PORTC);
	NVIC_ENABLE_IRQ(IRQ_PORTD);
	NVIC_ENABLE_IRQ(IRQ_PORTE);	
	*/
	
	// 2. SIM - enable clock : 18th bit of SIM_SCGC4 is USB OTG clock enable if ==1, before being set, reading USB registers will reboot
	SIM_SCGC4 |= SIM_SCGC4_USBOTG_MASK;
	
	//printUSBRegs("after clock usb");

	// 3. reset USB module if 7th bit of USB0_USBTRC0 == 1, cleared at reset
	USB0_USBTRC0 = USB_USBTRC0_USBRESET_MASK;
	while ((USB0_USBTRC0 & USB_USBTRC0_USBRESET_MASK) != 0) ; // wait for reset to end

#if defined(SERIAL_DEBUG)
	//printUSBRegs("after reset usb");
#endif	

	// 4. set desc table base addr
	USB0_BDTPAGE1 = ((uint32_t)table) >> 8;
	USB0_BDTPAGE2 = ((uint32_t)table) >> 16;
	USB0_BDTPAGE3 = ((uint32_t)table) >> 24;

	// 5. clear all ISR flags
	USB0_ISTAT = 0xFF;
	USB0_ERRSTAT = 0xFF;
	USB0_OTGISTAT = 0xFF;

	USB0_USBTRC0 |= 0x40; // undocumented bit !!!! the 6th bit is reserved....

	// enable USB : set USB0_CTL bit 1 to 1
	USB0_CTL = USB_CTL_USBENSOFEN_MASK;
	// enable weak pulldowns (bit 6 to 1)
	USB0_USBCTRL = 0; //0x40;

	// 6. enable reset interrupt : USBRST Interrupt Enable (0x1)
	USB0_INTEN = USB_INTEN_USBRSTEN_MASK;

	// enable interrupt in NVIC...
	NVIC_SET_PRIORITY(IRQ_USBOTG, 112);
	NVIC_ENABLE_IRQ(IRQ_USBOTG);

	// enable d+ pullup by setting bit 5 of USB0_CONTROL (DP Pullup in non-OTG device mode is enabled.)
	USB0_CONTROL = USB_CONTROL_DPPULLUPNONOTG_MASK;
	
#if defined(SERIAL_DEBUG)
	printUSBRegs("after pullup");
#endif		
}

/*
 * Debug stuff
 * 
 */
 
#if defined(SERIAL_DEBUG)
void xprintBits(char * regName, uint32_t size, volatile void * ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;
	
	xprintf("%s\t 0x%X \t: ", regName, ptr);

    for (i=size-1;i>=0;i--)
    {
        for (j=7;j>=0;j--)
        {
            byte = b[i] & (1<<j);
            byte >>= j;
            xprintf("%u", byte);
        }
    }
	xprintf("\r\n");
}

void printUSBRegs(char * step)
{
	uint8_t * irqOTGAdr = (uint8_t *)0x00000164;
	
	xprintf("----- USB Registers Dump : %s -----\r\n", step);
	xprintBits("SIM_SCGC4", 4, &SIM_SCGC4);
	xprintBits("SIM_SOPT2", 4, &SIM_SOPT2);
	
	xprintBits("USB0_USBTRC0", 1, &USB0_USBTRC0);
	xprintBits("USB0_BDTPAGE1", 1, &USB0_BDTPAGE1);
	xprintBits("USB0_BDTPAGE2", 1, &USB0_BDTPAGE2);
	xprintBits("USB0_BDTPAGE3", 1, &USB0_BDTPAGE3);
	xprintBits("USB0_ISTAT", 1, &USB0_ISTAT);
	xprintBits("USB0_ERRSTAT", 1, &USB0_ERRSTAT);
	xprintBits("USB0_OTGISTAT", 1, &USB0_OTGISTAT);
	xprintBits("USB0_USBTRC0", 1, &USB0_USBTRC0);
	xprintBits("USB0_CTL", 1, &USB0_CTL);
	xprintBits("USB0_USBCTRL", 1, &USB0_USBCTRL);
	xprintBits("USB0_INTEN", 1, &USB0_INTEN);
	xprintBits("USB0_CONTROL", 1, &USB0_CONTROL);
	xprintBits("IRQ_USBOTG", 4, irqOTGAdr);
	
	xprintf("---------------------------------------------------\r\n");

	
}
#endif
