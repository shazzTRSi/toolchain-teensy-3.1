/*
  * Teensyduino Core Library
 *  http://www.pjrc.com/teensy/
 *  Copyright (c) 2013 PJRC.COM, LLC.
 *
 */

#ifndef USBserial_h_
#define USBserial_h_

#include <inttypes.h>

int 	usb_serial_getchar(void);
int 	usb_serial_peekchar(void);
int 	usb_serial_available(void);
int 	usb_serial_read(void *buffer, uint32_t size);
void 	usb_serial_flush_input(void);
int 	usb_serial_putchar(uint8_t c);
int 	usb_serial_write(const void *buffer, uint32_t size);
void 	usb_serial_flush_output(void);

#define USB_SERIAL_DTR  0x01
#define USB_SERIAL_RTS  0x02

#endif // USBserial_h_
